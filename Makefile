CC = arm-none-eabi-gcc
AS = arm-none-eabi-as
OBJCOPY = arm-none-eabi-objcopy
CFLAGS = -g -O0 -T $(FIRMWARE)/flash.ld -fdata-sections -ffunction-sections -Wall -Wextra -DSTM32F401xE -mcpu=cortex-m4 -nostdlib -nostartfiles -specs=nosys.specs
ASFLAGS = -mcpu=cortex-m4
LINKFLAGS = -g -mcpu=cortex-m4 -T $(FIRMWARE)/flash.ld -Wl,--gc-sections

CUBE_F4 ?= ../STM32CubeF4
HAL := $(CUBE_F4)/Drivers/STM32F4xx_HAL_Driver
BSP := $(CUBE_F4)/Drivers/BSP/STM32F4xx-Nucleo
CMSIS_INCLUDE := -I$(CUBE_F4)/Drivers/CMSIS/Include -I$(CUBE_F4)/Drivers/CMSIS/Device/ST/STM32F4xx/Include

FIRMWARE := firmware
BUILD := build

INCLUDE := -I$(HAL)/Inc $(CMSIS_INCLUDE) -I$(BSP) -IInc

HAL_OBJECTS := stm32f4xx_hal.o stm32f4xx_hal_cortex.o stm32f4xx_hal_rcc.o stm32f4xx_hal_gpio.o stm32f4xx_hal_spi.o stm32f4xx_hal_dma.o
BSP_OBJECTS := stm32f4xx_nucleo.o
OBJECTS := main.o stm32f4xx_hal_msp.o stm32f4xx_it.o system_stm32f4xx.o exit_stub.o


vpath %.o $(BUILD)
vpath %.bin $(BUILD)
vpath %.elf $(BUILD)

all: clean flash.bin

$(HAL_OBJECTS):
	$(CC) -c $(CFLAGS) $(INCLUDE) $(@:%.o=$(HAL)/Src/%.c) -o $(BUILD)/$@

$(BSP_OBJECTS):
	$(CC) -c $(CFLAGS) $(INCLUDE) $(@:%.o=$(BSP)/%.c) -o $(BUILD)/$@

$(OBJECTS):
	$(CC) -c $(CFLAGS) $(INCLUDE) $(@:%.o=Src/%.c) -o $(BUILD)/$@

clean:
	rm -f $(BUILD)/*

startup.o:
	$(AS) $(ASFLAGS) $(FIRMWARE)/startup_stm32f401xe.s -o $(BUILD)/$@

image.elf:  startup.o $(HAL_OBJECTS) $(BSP_OBJECTS) $(OBJECTS)
	$(CC) $(LINKFLAGS) $(BUILD)/*.o -o $(BUILD)/$@

flash.bin: image.elf
	$(OBJCOPY) $(BUILD)/image.elf -Obinary $(BUILD)/$@

ST_FLASH = st-flash
run: flash.bin
	$(ST_FLASH) write $(BUILD)/flash.bin 0x08000000
